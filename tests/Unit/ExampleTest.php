<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
//        $this->assertTrue(true);
//        $this->visit('/api/test')
//            ->see('done')
//            ->dontSee('Rails');

        $this->get('/api/test')
            ->seeJson([
                'success' => true,
                'data' => 'done'
            ]);
    }
}
