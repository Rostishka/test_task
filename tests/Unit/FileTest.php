<?php

namespace Tests\Unit;

use App\Models\File\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use SebastianBergmann\Environment\Console;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FileTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return int
     */
    public function testUploadFile()
    {
        $stub = __DIR__ . '/../TestFiles/art.jpg';
        $name = 'testFileBeforeUpdate.jpg';
        $path = sys_get_temp_dir() . '/' . $name;
        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);
        $response = $this->call('PUT', '/api/file', [], [], ['uploadFile' => $file], ['Accept' => 'application/json']);

        $response
            ->assertStatus(200);
        $content = json_decode($response->getContent());
        $this->assertObjectHasAttribute('id', $content->data);

        $this->assertFileExists($content->data->path);
        echo "testUploadFile finished";
    }

    public function testGetFiles()
    {
        $response = $this->call('GET', '/api/file/list');
        $response
            ->assertStatus(200);
        $content = json_decode($response->getContent());
        $this->assertObjectHasAttribute('data', $content);
        $this->assertObjectHasAttribute('id', $content->data[0]);
        echo "\r\n";
        echo "testGetFiles finished";
    }

    public function testUpdateFile()
    {
        $fileFromDb = File::findOrFail(['name', 'testFileBeforeUpdate.jpg']);
        $stub = __DIR__ . '/../TestFiles/update.jpg';
        $name = 'testFileAfterUpdate.jpg';
        $path = sys_get_temp_dir() . '/' . $name;
        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);
        $response = $this->call('POST', '/api/file/' . $fileFromDb->id, [], [], ['uploadFile' => $file], ['Accept' => 'application/json']);

        $response
            ->assertStatus(200);
        $content = json_decode($response->getContent());
        $this->assertObjectHasAttribute('id', $content->data);
        $fileFromDb = File::findOrFail($content->data->id);
        $this->assertEquals('testFileAfterUpdate.jpg', $fileFromDb->name);

        $this->assertFileExists($content->data->path);

        echo "\r\n";
        echo "testUpdateFile finished";
    }

    public function testGetMetadataFromFile()
    {
        $fileFromDb = File::findOrFail(['name', 'testFileAfterUpdate.jpg']);
        $response = $this->call('GET', '/api/file/'. $fileFromDb->id .'/meta');
        $response
            ->assertStatus(200);
        $content = json_decode($response->getContent());
        $this->assertObjectHasAttribute('data', $content);
//        var_dump($content->data);
//        die();
        $this->assertObjectHasAttribute('filesize', $content->data);

        echo "\r\n";
        echo "testGetMetadataFromFile finished";
    }

    public function testRemoveFile()
    {
        $fileFromDb = File::findOrFail(['name', 'testFileAfterUpdate.jpg']);
        $response = $this->call('DELETE', '/api/file/'. $fileFromDb->id);
        $response
            ->assertStatus(200);
        $this->assertFileNotExists($fileFromDb->path);
        echo "\r\n";
        echo "testRemoveFile finished";
    }

}
