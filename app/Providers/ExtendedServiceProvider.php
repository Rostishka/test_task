<?php

namespace App\Providers;


use App\Extended\Database\Connectors\ConnectionFactory;
use Illuminate\Support\ServiceProvider;
use App\Extended\Database\MySqlConnection as ExtendedMySqlConnection;
use App\Extended\Routing\Router as ExtendedRouter;
use Illuminate\Foundation\Application;

class ExtendedServiceProvider extends ServiceProvider
{
    /**
     * Регистрация расширений сервисов.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton('router', function ($app)
        {
            return new ExtendedRouter($app['events'], $app);
        });
    }
}
