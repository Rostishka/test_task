<?php

namespace App\Console\CustomCommands;


use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;


/**
 * Очищает базу, запускает миграции и сиды.
 *
 * @package App\Console\Commands
 */
class CommandDBInit extends Command
{
    use ConfirmableTrait;

    /**
     * Сигнатура консольной команды.
     *
     * @var string
     */
    protected $signature = 'db:init {--force}';

    /**
     * Описание консольной команды.
     *
     * @var string
     */
    protected $description = 'Clear DB, run migration and seeds';

    /**
     * Файл лога.
     *
     * @var string
     */
    protected $logFile = 'db-init.log';

    /**
     * Запускает команду.
     */
    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return;
        }

        // Log::useDailyFiles(storage_path() . '/logs/' . $this->logFile);

        Log::info('db:init launched');

        $tables = [];


        Log::info('Loading list tables');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach (DB::select('SHOW TABLES') as $k => $v)
        {
            $tables[] = array_values((array) $v)[0];
        }

        Log::info('Found ' . count($tables) . ' tables');

	if(count($tables) > 0)
	    DB::statement('DROP TABLE IF EXISTS ' . implode(',', $tables) . ';');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Log::info('Migrations launch');
        $this->call('migrate', ['--force' => true]);


        Log::info('Seeders launch');
        $this->call('db:seed', ['--force' => true]);

        //start listener
        //queue:listen file-loads --tries=3
    }
}
