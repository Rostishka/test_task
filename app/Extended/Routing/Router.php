<?php

namespace App\Extended\Routing;


use Illuminate\Routing\Router as BaseRouter;
use App\Extended\Routing\Route as ExtendedRoute;

class Router extends BaseRouter
{

    /**
     * Create a new Route object.
     *
     * @param  array|string  $methods
     * @param  string  $uri
     * @param  mixed  $action
     * @return \Illuminate\Routing\Route
     */
    protected function newRoute($methods, $uri, $action)
    {
        return (new ExtendedRoute($methods, $uri, $action))
            ->setRouter($this)
            ->setContainer($this->container);
    }
}