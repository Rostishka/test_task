<?php

namespace App\Extended\Routing;


use App\Http\Controllers\Base\CrmController;
use Illuminate\Http\Request;
use Illuminate\Routing\Route as BaseRoute;
use Illuminate\Support\Facades\App;
use ReflectionMethod;

class Route extends BaseRoute
{
    /**
     * Run the route action and return the response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function run()
    {
        if (isset($this->action['class'], $this->action['method']))
        {
            return $this->runController();
        }
        else
            return parent::run();
    }

    /**
     * Get the parameters that are listed in the route / controller signature.
     *
     * @param string|null  $subClass
     * @return array
     */
    public function signatureParameters($subClass = null)
    {
        $action = $this->getAction();

        if (isset($action['class'], $action['method']))
        {
            $parameters = (new ReflectionMethod($action['class'], $action['method']))->getParameters();

            return is_null($subClass) ? $parameters : array_filter($parameters, function ($p) use ($subClass) {
                return $p->getClass() && $p->getClass()->isSubclassOf($subClass);
            });

        } else
            return parent::signatureParameters($subClass);
    }

    /**
     * Run the route action and return the response.
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function runController()
    {
        if (isset($this->action['class'], $this->action['method']))
        {
            /** @var CrmController $controller */
            $controller = App::make($this->action['class']);

            return $controller->call($this->action['method']);
        }
        return parent::runController();
    }


}