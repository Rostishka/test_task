<?php

namespace App\Extended\Http;


use Illuminate\Http\Request as BaseRequest;
use Illuminate\Support\Facades\Route;

/**
 * Расширение стандартного Request для легкого доступа к параметрам запроса.
 *
 * @package App\Extended\Http
 */
class Request extends BaseRequest
{
    /**
     * Параметры запроса.
     *
     * @var array
     */
    protected $params = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = array(), array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        $this->params = [
            'path' => Route::current()->parameters(),
            'query' => $this->request->query->all(),
            'formData' => $this->request->method() != 'GET'
                ? $this->request->request->all()
                : [],
            'file' => $this->request->files->all(),
        ];
    }

    /**
     * Получает значение параметра определенного типа по имени.
     *
     * @param string $type Тип параметра
     * @param string $key Ключ параметра
     * @param mixed $default Значение по умолчанию
     *
     * @return mixed
     */
    private function getParam($type, $key, $default)
    {
        if (isset($this->params[$type][$key]))
            return $this->params[$type][$key];

        return $default;
    }

    /**
     * Возвращает параметры всех типов в виде одного массива.
     *
     * @return array
     */
    public function getRequestParams()
    {
        return $this->params;
    }

    /**
     * Получает значение query параметра.
     *
     * @param string $key Ключ параметра
     * @param mixed|null $default Значение по умолчанию
     *
     * @return mixed|null
     */
    public function getQueryParam($key, $default = null)
    {
        return $this->getParam('query', $key, $default);
    }

    /**
     * Получает значение path параметра.
     *
     * @param string $key Ключ параметра
     * @param mixed|null $default Значение по умолчанию
     *
     * @return mixed|null
     */
    public function getPathParam($key, $default = null)
    {
        return $this->getParam('path', $key, $default);
    }

    /**
     * Получает значение formData параметра.
     *
     * @param string $key Ключ параметра
     * @param mixed|null $default Значение по умолчанию
     *
     * @return mixed|null
     */
    public function getFormDataParam($key, $default = null)
    {
        return $this->getParam('formData', $key, $default);
    }

    /**
     * Получает значение file параметра.
     *
     * @param string $key Ключ параметра
     * @param mixed|null $default Значение по умолчанию
     *
     * @return mixed|null
     */
    public function getFileParam($key, $default = null)
    {
        return $this->getParam('file', $key, $default);
    }

    /**
     * Получает массив query параметров.
     *
     * @return array
     */
    public function getQueryParams()
    {
        return isset($this->params['query'])
            ? $this->params['query']
            : [];
    }

    /**
     * Получает массив path параметров.
     *
     * @return array
     */
    public function getPathParams()
    {
        return isset($this->params['path'])
            ? $this->params['path']
            : [];
    }

    /**
     * Получает массив formData параметров.
     *
     * @return array
     */
    public function getFormDataParams()
    {
        return isset($this->params['formData'])
            ? $this->params['formData']
            : [];
    }

    /**
     * Получает массив file параметров.
     *
     * @return array
     */
    public function getFileParams()
    {
        return isset($this->params['file'])
            ? $this->params['file']
            : [];
    }
}
