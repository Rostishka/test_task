<?php

namespace App\Models\File;

use App\Models\BaseModel;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


/**
 * Файл.
 *
 * @property int id Идентификатор
 * @property string name Название файла
 * @property string path Путь до файла
 * @property string meta_data Идентификатор контрагента
 * @property int common_product_id Мета данные файла
 *
// * @property Client client Контрагент

 *

 *
 * Представления:
 * @method static array viewEditFormToSingleContragent($task, $fields) Представление формы редактирования задачи для одного контрагента
 *
 * @package App\Models\Tasks
 */
class File extends BaseModel
{

    #region Properties

    /**
     * {@inheritdoc}
     */
    protected $table = 'files_info';


//    protected static $viewsClass = HoldingView::class;

//    public static $tableKey = 'Holdings';
    /**
     * {@inheritdoc}
     */
    protected $primaryKey = 'id';

    #endregion Properties

    #region Relations

    //@return \Illuminate\Database\Eloquent\Relations\belongsTo
//    public function fileType()
//    {
//        return $this->belongsTo(Client::class, 'client_id');
//    }

    #endregion
}
