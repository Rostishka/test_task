<?php

namespace App\Models;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InternalServer\InternalServerException;
use App\Exceptions\Permission\PermissionException;
use App\Formatters\Tables\FilterFormatter;
use App\Models\Security\User;
use App\PermissionEloquentBuilder;
use App\Exceptions\InternalServer\SearchRuleException;
use App\Formatters\Formatter;
use App\Http\Helpers;
use App\Http\Middleware\SecurityService;
use App\Http\Security\PolicyServices;
use App\Traits\Pagination;
use App\Views\Base\BaseViewV1;
use App\Views\Base\iViewV3;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\DB;
use ReflectionClass;

/**
 * Базовая модель.
 *
 * @package App\Models\Base
 */
class BaseModel extends Model
{
    use SoftDeletes;

    /**
     * Название таблицы.
     *
     * @var string
     */
    protected $table;

    /**
     * Название таблицы.
     *
     * @var string
     */
    public static $tableName;

    /**
     * Имя первичного ключа.
     *
     * @var string
     */
    protected $primaryKey;

    /**
     * Поля доступные при массовом заполнении.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Кэш соответствия ключей и идентификаторов сущностей.
     * Формат: ['Model' => ['key' => id]]
     *
     * @var array
     */
    private static $byKeys = [];

    /**
     * Поля запрещенные при массовом заполнении.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Поля доступные при преобразовании в JSON.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Поля недоступные при преобразовании в JSON.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Использовать поля created_at и updated_at.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;


    public function __construct(array $attributes = [])
    {
        if(isset(static::$tableName)) {
            $this->table = static::$tableName;
        }

        parent::__construct($attributes);
    }

    /**
     * Возвращает инкрементируемый порядок
     * @param $order
     * @param bool $max
     * @return int
     */
    public static function getOrder(&$order, $max = false){
        $order += 10;
        return $order + 100;
    }

    protected static function boot()
    {
        parent::boot();
    }


    public function updateIfChanged($record)
    {
        $updateRecord = [];

        foreach ($record as $key=>$value)
        {
            if ($this->{$key} != $value)
                $updateRecord[$key] = $value;
        }

        //var_dump($updateRecord); die();
        $this->update($updateRecord);
    }

    /**
     * Возвращает массив идентификаторов существующих сущностей данного класса.
     *
     * @return array
     */
    public static function getExistingIds()
    {
        return static::select('id')->get()->pluck('id')->toArray();
    }

    /**
     * @param array|int $arg Идентификатор сущности или массив [field, value]
     * @return static
     * @throws EntityNotFoundException
     */
    public static function findOrFail($arg)
    {
        if(is_array($arg))
            $entity = self::where($arg[0], $arg[1])->first();
        else
            $entity = self::find($arg);

        if(!isset($entity)) {
            if (is_array($arg))
                throw EntityNotFoundException::by(static::class, $arg[0], $arg[1]);
            else
                throw EntityNotFoundException::byId(static::class, $arg);
        }

        return $entity;
    }

    /**
     * Возвращает идентификатор сущности по ключу.
     *
     * @param $key
     *
     * @return null
     */
    public static function byKey($key)
    {
        // Если есть в кэше то возвращаем его
        if (isset(self::$byKeys[static::class][$key]))
            return self::$byKeys[static::class][$key];

        // Если нет в кэше, то пытаемся найти
        $instance = static::where('key', $key)->first();

        // TODO: тут можно закэшировать и экземпляр раз всеравно его получили.

        if (isset($instance))
        {
            self::$byKeys[static::class][$key] = $instance->id;

            return $instance->id;
        }

        return null;
    }
}
