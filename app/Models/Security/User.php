<?php

namespace App\Models\Security;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * Пользователь.
 *
 * @property int id Идентификатор пользователя
 * @property string name ФИО пользователя
 * @property string email Email пользователя
 * @property string password Пароль
 * @property string remember_token Токен пользователя
 *
 *
 * @package App\Models\Security
 */

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
