<?php

namespace App;


use Illuminate\Routing\Router;

class ApiRouter extends Router
{

    /**
     * Create a new Route object.
     *
     * @param  array|string  $methods
     * @param  string  $uri
     * @param  mixed  $action
     * @return \Illuminate\Routing\Route
     */
    protected function newRoute($methods, $uri, $action)
    {
        return (new ApiRoute($methods, $uri, $action))
            ->setRouter($this)
            ->setContainer($this->container);
    }


}