<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator as FacadeValidator;
use Illuminate\Validation\Validator;

class ApiBaseController extends BaseController
{



    public function __construct()
    {
        $this->request = request();
//        $this->user = Auth::user();
    }

    public function response($data)
    {
        return response()->json([
            "success" => true,
            "data" => $data
        ], 200, []);
    }

    public function responseFalse($errors)
    {
        return response()->json([
            "success" => false,
            "errors" => $errors
        ], 400, []);
    }


    /**
     * Точка входа.
     * Вызывает метод контроллера.
     *
     * @param string $method Название метода.
     *
     * @return mixed
     */
    public function call($method)
    {
        // Получаем входные параметры из запроса
        $requestParams = $this->getRequestParams();

        // Получаем аннотацию метода
        $annotation = $this->getAnnotation($method);

        // Получаем из аннотации правила валидации
        $validationRules = $this->getValidationRules($annotation);

        // Валидируем запрос на наличие лишних параметров
        $this->validateUnnecessaryParams($requestParams, $validationRules);

        // Получаем из запроса входные параметры для метода контроллера
        $methodParams = $this->getMethodParams($method, $requestParams, $validationRules);

        // Проводим базовую валидацию параметров как "целое"
        $this->validateParams($methodParams, $validationRules);

        // Парсим json атрибуты
        $methodParams = $this->parseJson($methodParams, $validationRules);

        // Валидируем содержимое json параметров
        $this->validateJson($methodParams, $validationRules);

        return call_user_func_array(array($this, $method), $methodParams);
    }

    /**
     * Получает из запроса path, query, formData, file параметры.
     *
     * @return array
     */
    private function getRequestParams()
    {
        $requestParams = [
            'path' => Route::current()->parameters(),
            'query' => $this->request->query->all(),
            'formData' => $this->request->method() != 'GET'
                ? $this->request->request->all()
                : [],
            'file' => $this->request->files->all(),
        ];

        return $requestParams;
    }

    /**
     * Валидирует запрос на наличие лишних параметров.
     *
     * @param array $requestParams Параметры переданные в запросе
     * @param array $validationRules Правила валидации
     *
     * @throws ValidationException
     */
    private function validateUnnecessaryParams($requestParams, $validationRules)
    {
        foreach (['path', 'query', 'formData', 'file'] as $type)
        {
            foreach (array_keys($requestParams[$type]) as $requestParam)
            {
                if (!isset($validationRules[$requestParam]) || $validationRules[$requestParam]['source'] !== $type)
                    throw ValidationException::param($requestParam, "В запросе передан не описанный {$type} параметр {$requestParam}");
            }
        }
    }

    /**
     * Получает параметры для метода контроллера.
     *
     * @param string $method Название метода.
     * @param array $requestParams Параметры переданные в запросе
     * @param array $validationRules Правила валидации
     *
     * @return array
     */
    private function getMethodParams($method, $requestParams, $validationRules)
    {
        $params = [];

        $reflection = new ReflectionMethod(get_class($this), $method);

        foreach ($reflection->getParameters() as $param)
        {
            // Определяем источник параметра
            $source = isset($validationRules[$param->name])
                ? $validationRules[$param->name]['source']
                : null;

            // Если в запросе передан необходимы параметр то подставляем его значение
            if (isset($requestParams[$source][$param->name]))
                $params[$param->name] = $requestParams[$source][$param->name];
            // Если параметр не обязательный то подставляем значение по умолчанию
            elseif ($param->isOptional())
                $params[$param->name] = $param->getDefaultValue();
            // Иначе подставляем null
            else
                $params[$param->name] = null;
        }

        return $params;
    }

    /**
     * Валидирует параметры как "целое"
     *
     * @param array $methodParams Параметры для метода контроллера
     * @param array $validationRules Правила валидации
     *
     * @throws BadRequestException
     */
    private function validateParams($methodParams, $validationRules)
    {
        foreach ($validationRules as $param => $rule)
        {
            $value = isset($methodParams[$param]) ? $methodParams[$param] : null;

            /** @var Validator $paramValidator */
            $paramValidator = $this->getValidationFactory()->make([$param => $value],
                [$param => $rule['paramRules']], [], []);
            $paramValidator->setAttributeNames([$param => $param]);

            if ($paramValidator->fails())
                throw ValidationException::param($paramValidator->getMessageBag()->getMessages());
        }
    }

    /**
     * Получает аннотацию метода.
     *
     * @param string $method Название метода
     *
     * @return array
     */
    private function getAnnotation($method)
    {
        $reflector = new ReflectionClass(static::class);
        $annotation = $reflector->getMethod($method)->getDocComment();
        $annotation = (new ToArray())->convert(new Tokens((new Tokenizer())->parse($annotation, [])));

        return $annotation;
    }

    /**
     * Получает правила валидации входных параметров из аннотации.
     *
     * @param array $annotation Аннотация метода
     * @return array Правила валидации параметров [
     *      '<paramName>' => [
     * 'source' => '<[path|query|formData|file]>',
     * 'paramRules' => '<validators>',
     * 'jsonRules' => [
     * 'structureRules' => [],
     * 'structureAttributes' => [],
     * 'customRules' => [],
     * 'customFields' => [],
     * 'customMessages' => [],
     * ]
     * ]
     * ]
     * @throws SearchRuleException
     */
    private function getValidationRules($annotation)
    {
        // TODO: Кэширование
        // TODO: Заменить Exception на аналог SearchRuleException

        $rules = [];

        // Перебираем аннотацию
        foreach ($annotation as $item)
        {
            // Если определено правило валидации параметра запроса
            if ($item instanceof \stdClass && $item->name === 'ParamValidation')
            {
                // Валидируем правила
                if (!isset($item->values) || !is_array($item->values) || count($item->values) < 3)
                    throw new SearchRuleException("Ошибка в правилах валидации");

                @list($param, $source, $paramRules, $json) = $item->values;

                if (!in_array($source, ['path', 'query', 'formData', 'file']))
                    throw new SearchRuleException(null, "Неизвестный источник параметра");

                $rules[$param] = [
                    'source' => $source,
                    'paramRules' => $paramRules,
                ];

                // Если для параметра определены правила валидации JSON
                if (isset($json))
                {
                    // Валидируем правила
                    if (!$json instanceof \stdClass || $json->name !== 'JsonValidation')
                        throw new SearchRuleException("Ошибка в правилах валидации json");

                    if (!isset($json->values) || !is_array($json->values))
                        throw new SearchRuleException("Ошибка в правилах валидации json");

                    $formFields = [];
                    // Если определен ключ формы то загружаем поля и их кастомные названия
                    if (isset($json->values['form']))
                    {
                        //TODO протестить код. пока добавлю "улучшенный" код, но в отдельной ветке, чтобы всё не уронить
                        if(strpos($json->values['form'], ',') > 0 ) {
                            $formVar = explode(',', $json->values['form']);
                            $forms = Form::with('fields')
                                ->whereIn('key', $formVar)
                                ->get();
                            $formFields = [];
                            foreach ($forms as $form) {
                                $formFields = array_merge($formFields, $form->fields->pluck('name', 'key')->toArray());
                            }
                        }
                        else {
                            /** @var Form $form */
                            $form = Form::with('fields')
                                ->where('key', $json->values['form'])
                                ->first();
                            $formFields = $form->fields->pluck('name', 'key')->toArray();
                        }

                    }

                    $structureRules = [];
                    $customRules = [];
                    $fields = [];
                    $customMessages = [];

                    // Перебираем определение правил, кастомные названия полей и кастомные сообщения
                    foreach ($json->values as $value)
                    {
                        if ($value instanceof \stdClass && isset($value->name))
                        {
                            if ($value->name === 'Rule')
                            {
                                if (!isset($value->values) || !is_array($value->values) || count($value->values) < 2)
                                    throw new SearchRuleException("Ошибка в правилах валидации json");

                                @list($attribute, $system, $custom) = $value->values;

                                if (!isset($structureRules[$attribute]))
                                    $structureRules[$attribute] = $system;
                                else
                                    $structureRules[$attribute] .= '|' . $system;

                                if (isset($custom))
                                {
                                    if (!isset($customRules[$attribute]))
                                        $customRules[$attribute] = $custom;
                                    else
                                        $customRules[$attribute] .= '|' . $custom;
                                }
                            }
                            elseif ($value->name === 'Field')
                            {
                                if (!isset($value->values) || !is_array($value->values) || count($value->values) !== 2)
                                    throw new SearchRuleException("Ошибка в правилах валидации json атрибутов");

                                @list($field, $alias) = $value->values;

                                $fields[$field] = $alias;
                            }
                            elseif ($value->name === 'Message')
                            {
                                if (!isset($value->values) || !is_array($value->values) || count($value->values) !== 2)
                                    throw new SearchRuleException("Ошибка в правилах валидации json сообщений");

                                @list($attribute, $message) = $value->values;

                                $customMessages[$attribute] = $message;
                            }
                        }
                    }

                    $customFields = [];
                    $structureAttributes = [];

                    // Перебирая структурные правила формируем кастомные названия атрибутов
                    foreach ($structureRules as $attribute => $rule)
                    {
                        $field = explode('.', $attribute)[0];

                        // Если в аннотации определено кастомное название поля то берем его
                        if (isset($fields[$field]))
                            $customFields[$attribute] = $fields[$field];
                        // Иначе если в базе определено кастомное название поля то берем его
                        elseif (isset($formFields[$field]))
                            $customFields[$attribute] = $formFields[$field];
                        // Иначе берем исходное название поля
                        else
                            $customFields[$attribute] = $attribute;

                        $structureAttributes[$attribute] = $attribute;
                    }
                    // TODO: добавлять к структурным правилам пользовательские правила
                    $rules[$param]['jsonRules'] = [
                        'structureRules' => $structureRules,
                        'structureAttributes' => $structureAttributes,
                        'customRules' => $customRules,
                        'customFields' => $customFields,
                        'customMessages' => $customMessages,
                    ];
                }
                else
                    $rules[$param]['jsonRules'] = [];

            }
        }

        // TODO: Вынести определение api_key правила
        $rules['api_key'] = ['source' => 'query', 'paramRules' => 'string', 'jsonRules' => []];
        $rules['apiKey'] = ['source' => 'query', 'paramRules' => 'string', 'jsonRules' => []];

        return $rules;
    }

    /**
     * Парсит json параметры.
     *
     * @param array $methodParams Параметры для метода контроллера
     * @param array $validationRules Правила валидации
     *
     * @return array
     */
    private function parseJson($methodParams, $validationRules)
    {
        $params = [];
        foreach ($methodParams as $param => $value)
        {
            // TODO: Автоматически добавлять к paramRules валидатор json если определены jsonRules.
            if (isset($validationRules[$param]) &&
                (count($validationRules[$param]['jsonRules']) > 0 || strpos($validationRules[$param]['paramRules'], 'json') !== false) &&
                isset($value)
            )
                $params[$param] = json_decode($value, true);
            else
                $params[$param] = $value;
        }

        return $params;
    }

    /**
     * Валидирует содержимое json параметров метода
     *
     * @param array $methodParams Параметры для метода контроллера
     * @param array $validationRules Правила валидации
     *
     * @throws BadRequestException
     */
    private function validateJson($methodParams, $validationRules)
    {
        foreach ($validationRules as $param => $rule)
        {
            if (is_array($rule['jsonRules']) && count($rule['jsonRules']) > 0)
            {
                $value = isset($methodParams[$param]) ? $methodParams[$param] : [];

                // Проводим структурную валидацию
                /** @var Validator $structureValidator */
                $structureValidator = FacadeValidator::make($value, $rule['jsonRules']['structureRules']);
                $structureValidator->setAttributeNames($rule['jsonRules']['structureAttributes']);
                // Проводим пользовательскую валидацию
                /** @var Validator $customValidator */
                $customValidator = FacadeValidator::make($value, $rule['jsonRules']['customRules'],
                    $rule['jsonRules']['customMessages']);

                $customValidator->setAttributeNames($rule['jsonRules']['customFields']);

                if ($structureValidator->fails() || $customValidator->fails())
                    throw new ValidationException($structureValidator->messages()->getMessages(),
                        $customValidator->messages()->getMessages());
            }
        }
    }
}
