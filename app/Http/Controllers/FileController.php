<?php

namespace App\Http\Controllers;

use App\Exceptions\MethodNotFoundException;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationException;
use App\Models\File\File;
use App\Models\Security\User;
use Carbon\Carbon;
use getID3;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileController
 *
 */
class FileController extends ApiBaseController
{
    public function test()
    {
        $filename = storage_path(). '/app/upload/7b83faa8855cc90d43b3547fda811df4.zip';
        $filename = storage_path(). '/app/upload/e730cd78f5620b3ee36057f86c208ee1.psd';
//        $filename = storage_path(). '/app/upload/b4ef0fd576e05a72ca2ff4b9a61005f0.jpg';

        $getID3 = new getID3();
        $info = $getID3->analyze($filename);
        unset($info['zip']);


        return $this->response($info);

    }


    /**
     * Создать файл из данных в запросе
     *
     * {@inheritdoc}
     *
     *
     */
    public function uploadFile(Request $request)
    {
        list($filename, $path, $fileOriginalName) = static::getFileFromRequest($request);

        $file = new File();
        $file->name = $fileOriginalName;
        $file->path = $path.$filename;
        $getID3 = new getID3();
        $file->meta_data = json_encode($getID3->analyze($path.$filename));
        //TODO add relation with current user, after adding AUTH
        //$fileInDb->user_id = $this->user->id;
        $file->save();

        return $this->response(['filename' => $filename, 'path' => $path, 'id' => $file->id]);

    }


    /**
     * Обновить содержимое файла из данных в запросе
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function updateFile(Request $request, $id)
    {
        list($filename, $path, $fileOriginalName) = static::getFileFromRequest($request);
        //TODO duplicate code wtf
        $file = File::findOrFail($id);
        unlink($file->path);
        $file->name = $fileOriginalName;
        $file->path = $path.$filename;
        $getID3 = new getID3();
        $file->meta_data = json_encode($getID3->analyze($path.$filename));
        $file->save();

        return $this->response(['filename' => $filename, 'path' => $path, 'id' => $file->id]);
    }


    /**
     * Получить содержимое файла
     * @param int $id
     */
    public function getFile($id)
    {
        $file = File::findOrFail($id);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($file->name).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Content-Length: ' . filesize($file->path));
        readfile($file->path);
    }


    /**
     * Получить метаданные файла
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMetadataFromFile($id)
    {
        $file = File::findOrFail($id);
        return $this->response(json_decode($file->meta_data));
    }


    /**
     * Получить список файлов
     */
    public function getFiles()
    {
        $files = File::all();
        return $this->response($files);
    }

    /**
     * Получить содержимое файла
     * @param int $id
     */
    public function removeFile($id)
    {
        $file = File::findOrFail($id);
        //TODO check file permission
        unlink($file->path);
        $file->delete();
    }


    private static function getFileFromRequest($request)
    {
        $uploadFile = (null !== $request->files->get('uploadFile'))
            ? $request->files->get('uploadFile')
            : null;
        $path = storage_path() .'/app/upload/';
        $fileOriginalName = $uploadFile->getClientOriginalName();
        $extension = explode('.', $fileOriginalName);
        $extension = $extension[count($extension) - 1];
        $filename = md5(Carbon::now() . $fileOriginalName) . '.'.$extension;
        $uploadFile->move($path, $filename);
        return [$filename, $path, $fileOriginalName];
    }

}
