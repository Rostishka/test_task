<?php

namespace App\Exceptions;

use App\Exceptions\BaseException;
use Exception;

/**
 * Ресурс не найден.
 *
 * @package App\Exceptions\NotFound
 */
abstract class NotFoundException extends BaseException
{
    public function __construct($message = "Not found", Exception $previous = null)
    {
        parent::__construct($message, 404, $previous);
    }
}