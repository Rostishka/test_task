<?php

namespace App\Exceptions;

use Exception;

/**
 * Ошибка валидации.
 *
 * @package App\Exceptions
 */
class ValidationException extends BaseException
{
    /**
     * ValidationException constructor.
     *
     * @param string $message
     */
    public function __construct($message = "Oops something goes wrong", Exception $previous = null)
    {
        parent::__construct($message, 400, $previous);
    }
}