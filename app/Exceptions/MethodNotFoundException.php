<?php

namespace App\Exceptions;
use Exception;

/**
 * Метод не найден.
 *
 * @package App\Exceptions\Base
 */
class MethodNotFoundException extends NotFoundException
{
    public function __construct($message = "Method not found", Exception $previous = null)
    {
        parent::__construct($message, $previous);
    }
}