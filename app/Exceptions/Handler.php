<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
        HttpException::class,
        ModelNotFoundException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return Response
     *
     * @throws InternalServerException
     * @throws MethodNotFoundException
     */
    public function render($request, Exception $exception)
    {
        $result = ["success" => false];

        $result['debug'] = [
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'exception' => get_class($exception),
            'message'=> $exception->getMessage(),
            'trace' => $exception->getTrace(),
        ];

        // Если ошибка "выброшена" внешним кодом
        if(!($exception instanceof BaseException))
        {
            // "Выбрасываем" соответствующее исключение
            if($exception instanceof NotFoundHttpException || $exception instanceof MethodNotAllowedHttpException)
                $exception = new MethodNotFoundException($exception);
            else
                $exception = new InternalServerException($exception);
        }

        // Если это ошибка валидации
        if ($exception instanceof ValidationException)
        {
            $result['message'] = $exception->getMessage();
//            $result['errors'] = $exception->getCustomErrors();
//            $result['structureErrors'] = $exception->getStructureErrors();
//            $result['paramErrors'] = $exception->getParamErrors();
        }
        // Если это любая другая ошибка
        else
            $result['message'] = $exception->getMessage();

        // Записываем ошибку в лог
//        return $result;
//        $result['errorCode'] = Error::log($result, $exception->getCode());

        // Возвращаем ошибку клиенту
        return response($result, $exception->getCode());
    }
}
