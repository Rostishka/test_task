<?php

namespace App\Exceptions;

/**
 * Ресурс не найден.
 *
 * @package App\Exceptions\NotFound
 */
class EntityNotFoundException extends NotFoundException
{
    public static function byId($type, $id)
    {
        $ref = new \ReflectionClass($type);

        return new EntityNotFoundException("Entity {$ref->getShortName()} with id {$id} not found.");
    }

    public static function by($type, $field, $value)
    {
        $ref = new \ReflectionClass($type);

        return new EntityNotFoundException("Entity {$ref->getShortName()} with {$field} {$value} not found.");
    }
}