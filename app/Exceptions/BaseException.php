<?php

namespace App\Exceptions;

/**
 * Базовое исключение.
 *
 * @package App\Exceptions
 */
abstract class BaseException extends \Exception
{

}