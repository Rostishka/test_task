<?php

namespace App\Exceptions;

use App\Exceptions\BaseException;
use Exception;

/**
 * Внутренняя ошибка сервера.
 *
 * @package App\Exceptions\InternalServer
 */
class InternalServerException extends BaseException
{
    public function __construct($message = "Some BAD Internal Server Error", Exception $previous = null)
    {
        parent::__construct($message, 500, $previous);
    }
}