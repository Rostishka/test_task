<?php

namespace App;


use App\Http\Controllers\ApiBaseController;
use Illuminate\Routing\ControllerDispatcher;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\App;
use ReflectionMethod;

class ApiRoute extends Route
{
    /**
     * Run the route action and return the response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function run()
    {
        if (isset($this->action['class'], $this->action['method']))
        {
            return $this->runController();
        }
        else
            return parent::run();
    }

    /**
     * Get the parameters that are listed in the route / controller signature.
     *
     * @param string|null  $subClass
     * @return array
     */
    public function signatureParameters($subClass = null)
    {
        $action = $this->getAction();

        if (isset($action['class'], $action['method']))
        {
            $parameters = (new ReflectionMethod($action['class'], $action['method']))->getParameters();

            return is_null($subClass) ? $parameters : array_filter($parameters, function ($p) use ($subClass) {
                return $p->getClass() && $p->getClass()->isSubclassOf($subClass);
            });

        } else
            return parent::signatureParameters($subClass);
    }

    /**
     * Run the route action and return the response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function runController()
    {
        if (isset($this->action['class'], $this->action['method']))
        {
            /** @var ApiBaseController $controller */
            $controller = App::make($this->action['class']);

            return $controller->call($this->action['method']);
        }
        return parent::runController($this->app['request']);
    }


}