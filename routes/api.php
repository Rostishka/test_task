<?php

use App\Http\Controllers\FileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::pattern('id', '[0-9]+');

//::middleware('auth:api')

Route::get('/test', 'FileController@test');

Route::group(['prefix' => 'file'], function () {
    Route::put('/', 'FileController@uploadFile');
    Route::post('/{id}', 'FileController@updateFile');
    Route::get('/{id}', 'FileController@getFile');
    Route::get('/list', 'FileController@getFiles');
    Route::get('/{id}/meta', 'FileController@getMetadataFromFile');
    Route::delete('/{id}', 'FileController@removeFile');
});
