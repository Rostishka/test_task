<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255)
                ->default('unknown_file_name')
                ->comment('Название файла');
            $table->string('path', 500)->unique()
                ->comment('Путь до файла');
            $table->text('meta_data')
                ->nullable()
                ->comment('Мета данные файла');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files_info');
    }
}
