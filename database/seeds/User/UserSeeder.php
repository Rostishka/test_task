<?php

use App\Models\Security\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        #region Пользователи из старой CRM

        $user = new User();
        $user->name = 'Пользователь для тестов';
        $user->email = 'test@test.ru';
        $user->password = Hash::make("test");;
        $user->save();

        #endregion Фейки
    }
}