## Test task for Xsolla by RostovskiiAlexandr

This project kind of little brother of the dropbox:


# Installation

### Clear install on empty server (ubuntu 16):
```sh
sudo apt-get install git
```

### Go to your future project folder
```sh
git clone https://bitbucket.org/Rostishka/test_task.git
```
then
```sh
cd test_task
sudo sh install.sh
composer install
cp .env.example .env
php artisan db:init
```

### Set in your php.ini (example data)
```sh
post_max_size=30M
upload_max_filesize=30M
```


### Example nginx settings
```sh
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        
        client_max_body_size 30m;
        client_body_timeout 120s;
        
        # dont forget to change it!!!
        server_name test;
        root /var/www/html/test/public;
        # dont forget to change it!!!
        
        index index.html index.htm index.php;

        access_log /var/www/html/logs/access.log;
        error_log /var/www/html/logs/error.log;


        location / {
                try_files $uri $uri/ /index.php;
        }

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.0-fpm.sock;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param SERVER_NAME $http_host;
        }

        location ~ /\.ht {
                deny all;
        }
}
```



If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Api
| PUT | /api/file/ |
| ------ | ------ |
| Description | upload file to the server |
| type of param | formData |
| params: |  |
| uploadFile | FILE |

| GET | /api/file/{id} |
| ------ | ------ |
| Description | get file from the server by id |

| POST | /api/file/{id} |
| ------ | ------ |
| Description | update file on the server by id |
| type of param | formData |
| params: |  |
| uploadFile | FILE |

| GET | /api/file/list |
| ------ | ------ |
| Description | get files  |

| GET | /api/file/{id}/meta |
| ------ | ------ |
| Description | get metadata from file by id  |

| DELETE | /api/file/{id} |
| ------ | ------ |
| Description | remove file from the server |


### TESTS
Run in project root folder
```sh
vendor/phpunit/phpunit/phpunit
```


## Used libraries
- [Laravel](https://laravel.com/)
- [getID3](https://github.com/JamesHeinrich/getID3)


# Кастально фич, которые я бы релизовал

### HTTP кэширование повторных вызовов получения данных
- как вариант memcached либо redis

### Механизм авторизации в апи
- Реализация авторизации сделает прототипчик сервиса по сохранению файлов более юзабельным. Добавление авторизации открывает широкие возможности, такие как: Разграничение доступа к файлам. Область видимости. Квоты на загрузки. Лимит. (нет я не читаю из файла задания, пишу оп памяти :D)
- использовал бы [Laravel Passport](https://laravel.com/docs/5.4/passport), точнее я даже пробовал, но за 10 минут оно не завелось, решил не тратить время, и так много потерял впустую. Либо рукописную тоже через токены.

### Привязка файла к клиенту-создателю и запрет на изменение для других клиентов.
- тут всё просто, добавляем связь User->File ManyToMany для добавления галочки "может править"
- в File добавляем связь owner_id который всегда имеет права на файл

### Квоты с местом и лимит для пользователя
- аналогично с предыдущим пунктом, вводим в модель User поле file_space (либо вариант интересней, вводим отдельную модель ответственную за "тариф" пользователя в которой указывается размер и разные параметры вида длительности, файлы в день, гигабайты в денть итд)
 

### Настроенный Vagrant/Docker/Chef/Puppet с веб-сервером, PHP и приложением
- последний раз когда работал связкой Vagrant-laravel скорость работы достигала 20 секунд ожидания любой апишки, насколько помню синхронизация всех библиотек из директории vendor не самая быстрая задача.

### Лимиты на размер загружаемых файлов.
- В теории nginx и php-fpm с настройками указанными выше сами разберутся, какие файлы можно загружать, но на деле не особо они вмешиваются в попытку загрузить гигабайтный файл 0_о


# ALARM

# Если вдруг автоматическая установка не сработала.
- Как вариант протестировать можно на серваке который арендовал на  digitalOcean для теста. В репозитории лежит файл xsolla.postman_collection.json для POSTMAN.
ip сервера там вбит

# ALARM